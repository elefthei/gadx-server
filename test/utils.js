/*jslint devel: true, browser: true, eqeq: true, es5: true, white: true, nomen:
 * true, maxerr: 499, indent: 2 */
/* eslint-env node, mocha */
const request = require('supertest');
const async = require('async');
const chai = require('chai');

const expect = chai.expect;

function testProduct(product, amount, server, done) {
  const agent = request.agent(server);
  [base, quote] = product.split("-")
  async.series(
      [
        // Send a /auth/token request
        function(cb) {

          // Make it late on purpose
          const requestBody = {
            "action" : "buy",
            "base_currency" : base,
            "quote_currency" : quote,
            "amount" : amount
          }

                              agent.post('/quote')
                                  .set('Content-Type', 'application/json')
                                  .send(requestBody)
                                  .expect(function(res) {
                                    expect(res.text).to.have.string('total');
                                    expect(res.text).to.have.string('price');
                                    expect(res.text).to.have.string('currency');
                                  })
                                  .expect(200, cb);
        }
      ],
      done);
};

function testNotProduct(product, server, done) {
  const agent = request.agent(server);
  [base, quote] = product.split("-")
  async.series(
      [
        // Send a /auth/token request
        function(cb) {

          // Make it late on purpose
          const requestBody = {
            "action" : "buy",
            "base_currency" : base,
            "quote_currency" : quote,
            "amount" : 1.00
          }

                              agent.post('/quote')
                                  .set('Content-Type', 'application/json')
                                  .send(requestBody)
                                  .expect(function(res) {
                                    expect(res.text).to.have.string(
                                        `Unsupported orderbook ${product}`);
                                  })
                                  .expect(400, cb);
        }
      ],
      done);
};

function testZeroAmountProduct(product, server, done) {
  const agent = request.agent(server);
  [base, quote] = product.split("-")
  async.series(
      [
        // Send a /auth/token request
        function(cb) {

          // Make it late on purpose
          const requestBody = {
            "action" : "buy",
            "base_currency" : base,
            "quote_currency" : quote,
            "amount" : 0.00
          }

                              agent.post('/quote')
                                  .set('Content-Type', 'application/json')
                                  .send(requestBody)
                                  .expect(function(res) {
                                    expect(res.text).to.have.string('amount');
                                  })
                                  .expect(400, cb);
        }
      ],
      done);
};

module.exports = {
  testProduct : testProduct,
  testNotProduct : testNotProduct,
  testZeroAmountProduct : testZeroAmountProduct
};
