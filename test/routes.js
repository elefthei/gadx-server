/*jslint devel: true, browser: true, eqeq: true, es5: true, white: true, nomen:
 * true, maxerr: 499, indent: 2 */
/* eslint-env node, mocha */
const request = require('supertest');
const async = require('async');
const path = require('path');
const chai = require('chai');

const expect = chai.expect;
const testutils = require('./utils');

describe('Testing GDAX server', function() {
  var app;
  var server;
  this.timeout(20000);

  // Launch server before testing
  before(function() {
    app = require('../app');
    // Port 3001 for testing
    server = app.listen(3001);
  });

  // Close server after testing
  after(function() { server.close(); });

  describe('Testing basic functionality', function() {
    it('test GET on root', function testSlash(done) {
      request(server)
          .get('/')
          .expect('content-type', 'text/html; charset=utf-8')
          .expect(function(res) {
            expect(res.text).to.have.string('It works');
          })
          .expect(200, done);
    });
  });

  describe('Testing existing products with quota endpoint', function() {
    it('Get ETH-USD quote', function(done) {
      testutils.testProduct('ETH-USD', 1.00, server, done);
    });
    it('Get USD-ETH quote', function(done) {
      testutils.testProduct('USD-ETH', 100.00, server, done);
    });
    it('Get USD-LTC quote', function(done) {
      testutils.testProduct('USD-LTC', 12.00, server, done);
    });
    it('Get LTC-USD quote',
       function(done) { testutils.testProduct('LTC-USD', 32, server, done); });
    it('Get BTC-LTC quote',
       function(done) { testutils.testProduct('BTC-LTC', 100, server, done); });
    it('Get LTC-BTC quote', function(done) {
      testutils.testProduct('LTC-BTC', 10000, server, done);
    });
    it('Get EUR-BTC quote', function(done) {
      testutils.testProduct('EUR-BTC', 1000, server, done);
    });
    it('Get BTC-EUR quote', function(done) {
      testutils.testProduct('BTC-EUR', 1.00, server, done);
    });
    it('Get BTC-ETH quote', function(done) {
      testutils.testProduct('BTC-ETH', 2.00, server, done);
    });
    it('Get ETH-BTC quote', function(done) {
      testutils.testProduct('ETH-BTC', 1337, server, done);
    });
  });

  describe('Testing non-existing products with quota endpoint', function() {
    it('Return 400 when getting a LTC-GBP quote',
       function(done) { testutils.testNotProduct('LTC-GBP', server, done); });
    it('Return 400 when getting a GBP-LTC quote',
       function(done) { testutils.testNotProduct('GBP-LTC', server, done); });
  });

  describe('Testing edge-cases on quota endpoint', function() {
    it('Return 400 when the amount is zero', function(done) {
      testutils.testZeroAmountProduct('USD-LTC', server, done);
    });
  });

});
