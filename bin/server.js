/*jslint devel: true, browser: true, eqeq: true, es5: true, white: true, nomen:
 * true, maxerr: 499, indent: 2 */
/*
 * Author: Lef Ioannidis
 */
const path = require('path');
const port = process.env.PORT || 3000;
const http = require('http');
const fs = require('fs');
const app = require('../app');

const gdax = require(path.join(global.__base, 'lib', 'gdax'));

// Create https server
const server = http.createServer(app);

// This event listener is to handle the signal
// sent by the debugger to 'kill` the nodejs process with an exit code of 0
process.on("SIGTERM", () => { process.exit(0); });

process.on('unhandledRejection', (reason, p) => {
  console.log('Unhandled Rejection at: Promise', p, 'reason:', reason);
  // application specific logging, throwing an error, or other logic here
});

/**
 * Event listener for HTTP server "error" event.
 */
function onError(error) {
  if (error.syscall !== 'listen') {
    throw error;
  }
  var bind = typeof port === 'string' ? 'Pipe ' + port : 'Port ' + port;

  // handle specific listen errors with friendly messages
  switch (error.code) {
  case 'EACCES':
    return new Error(bind + ' requires elevated privileges');
  case 'EADDRINUSE':
    return new Error(bind + ' is already in use');
  default:
    throw error;
  }
}

/**
 * Event listener for HTTP server "listening" event.
 */
function onListening() {
  var addr = server.address();
  var bind = typeof addr === 'string' ? 'pipe ' + addr : 'port ' + addr.port;
  console.log('Listening on ' + bind);
}

/**
 * Load Live Books, then the server
 **/
gdax.load()
    .then(() => {
      /**
      * Listen on provided port.
      **/
      app.set('port', port);
      server.listen(port);
      server.on('error', onError);
      server.on('listening', onListening);
    })
    .catch((err) => { throw err; });
