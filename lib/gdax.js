const GTT = require('gdax-trading-toolkit');
const LiveOrderbook = GTT.Core.LiveOrderbook;
const path = require('path');

const consoleLogger = require(path.join(global.__base, 'lib', 'logger'));

const supportedProducts =
    [
      'LTC-EUR', 'LTC-BTC', 'BTC-GBP', 'BTC-EUR', 'ETH-EUR', 'ETH-BTC',
      'LTC-USD', 'BTC-USD', 'ETH-USD'
    ]

    class GDAX {
  constructor() {
    this.loaded = false;
    this.books = {};
  }

  // Load all LiveBooks
  load() {
    // Return fast if already initialized
    if (this.loaded) {
      return Promise.resolve();
    }
    // set it to true
    this.loaded = true;
    var _self = this;
    // Create the GDAX feed
    return GTT.Factories.GDAX.FeedFactory(consoleLogger).then((feed) => {
      // Create a dictionary of Live books
      return Promise.all(supportedProducts.map((product) => {
        return new Promise(function(resolve, reject) {
          // Configure the live book object
          const config = {product : product, logger : consoleLogger};
          let book = new LiveOrderbook(config);
          book.on('LiveOrderbook.snapshot', () => {
            consoleLogger.log('debug', `Snapshot received for ${product}`);
            resolve(book);
          });
          book.on('end', () => {
            consoleLoger.log('info', 'Book closed');
            resolve(book);
          });
          book.on('error', (err) => {
            consoleLogger.log('error', 'Book errored: ', err);
            reject(err);
            feed.pipe(book);
          });
          feed.pipe(book);
          _self.books[product] = book;
        });
      }));
    })
  };

  // Get a quote for a trade
  //
  // @product: ie: USD-BTC, ETH-BTC etc
  // @amount: 1000.0
  // @use_quote: True|False
  // @callback err
  // @callback cb
  getQuote(type, product, amount, use_quote) {
    var _self = this;
    return new Promise(function(resolve, reject) {
             // Get quote for given order
             let orders =
                 _self.books[product].ordersForValue(type, amount, use_quote);
             // Return these to client
             let total = orders[orders.length - 1].cumValue.toFixed(2)
                             let price = total / amount
             let currency = _self.books[product].quoteCurrency
             consoleLogger.log(
                 'debug',
                 `Cost of buying ${amount} ${_self.books[product].baseCurrency}: ${orders[orders.length - 1].cumValue.toFixed(2)} ${_self.books[product].quoteCurrency}`);
             resolve({'total' : total, 'price' : price, 'currency' : currency});
           })
        .catch((error) => {
          consoleLogger.log('error', error);
          reject(error);
        });
  };

  handleTrade(action) {
    var _self = this;
    return new Promise(function(resolve, reject) {
      // Ensure quote and base currency pair are in the supported products
      const normal_trade = action.base + '-' + action.quote

      // Create correct productID by matching the order against available
      // orderbooks
      if (supportedProducts.indexOf(normal_trade) < 0) {
        const reverse_trade = action.quote + '-' + action.base
        if (supportedProducts.indexOf(reverse_trade) < 0) {
          reject(`Unsupported orderbook ${normal_trade}`);
        }
        else {
          // Reverse the order to match available orderbook
          let type = ((action.type === 'buy') ? 'sell' : 'buy')
          _self.getQuote(type, reverse_trade, action.amount, true)
              .then((result) => {resolve(result)})
              .catch((err) => {reject(err)});
        }
      }
      else {
        _self.getQuote(action.type, normal_trade, action.amount, false)
            .then((result) => {resolve(result)})
            .catch((err) => {reject(err)});
      }
    });
  };
}

module.exports = new GDAX();
