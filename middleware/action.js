const path = require('path');

module.exports = class ActionMiddleware {
  // Encofrce and parse `action` schema
  static parse(req, res, next) {
    let json = req.body;
    if (!json.action || (json.action !== 'buy' && json.action !== 'sell')) {
      return res.status(400).json({
        'code' : 400,
        'error' : 'Incomplete request',
        'message' : 'Missing `action` field',
      });
    }
    if (!json.base_currency || json.base_currency == "") {
      return res.status(400).json({
        'code' : 400,
        'error' : 'Incomplete request',
        'message' : 'Missing `base_currency` field',
      });
    }
    if (!json.quote_currency || json.quote_currency == "") {
      return res.status(400).json({
        'code' : 400,
        'error' : 'Incomplete request',
        'message' : 'Missing `quote_currency` field',
      });
    }
    if (!json.amount || json.amount == "" || json.amount == 0) {
      return res.status(400).json({
        'code' : 400,
        'error' : 'Incomplete request',
        'message' : 'Missing `amount` field',
      });
    }
    req.action = {
      'type' : json.action,
      'base' : json.base_currency,
      'quote' : json.quote_currency,
      'amount' : json.amount
    }

    return next();
  };
}
