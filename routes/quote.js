var express = require('express');
var path = require('path');
var request = require('request');
var router = express.Router();

// Load middleware functions
const ActionMiddleware =
    require(path.join(global.__base, 'middleware', 'action'));
const gdax = require(path.join(global.__base, 'lib', 'gdax'));
const logger = require(path.join(global.__base, 'lib', 'logger'));

/**
* POST /quote
*
* Returns a quote for an exchange
*
*/
router.post('/', ActionMiddleware.parse, (req, res) => {
  const action = req.action;

  gdax.load().then(() => {
    logger.log('debug', action);
    return gdax.handleTrade(action)
        .then((result) => {
          logger.log('debug', `Result of GADX quote: ${result}`);
          return res.status(200).json(result);
        })
        .catch((err) => {
          logger.log('error', err);
          return res.status(400).json({
            'code' : 400,
            'error' : 'Incomplete request',
            'message' : err,
          });
        });
  });
});

module.exports = router;
