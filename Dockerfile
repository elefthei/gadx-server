 # Set the base image to node:6.x (debian linux jessie)
FROM node:8

MAINTAINER Lef Ioannidis <rausted@gmail.com>

# Install missing dependencies
# Create docker user
# Kludge: Docker user is given uid 999 as a workaround on Debian so that it can
# write to the mounted repository
RUN mkdir -p /home/node/src

# Copy package.json first to use docker layer caching
ADD . /home/node/src
RUN chown -R node:node /home/node

WORKDIR /home/node/src
USER node

RUN npm install -q
EXPOSE  3000

# Disables ExpressJS sending a stack-trace to the client
ENV NODE_ENV production

CMD [ "node", "./bin/server.js" ]
