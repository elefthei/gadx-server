# gdax-server
A microservice for getting cryptocurrency values from GDAX, uses GDAX trading toolkit
=======
# GADX Trading Server

Server for getting quotes for GADX orders.

## Instructions
### Installation
* Currently running Node.js v6.3.0 on Docker 1.12.0
* Initialize `npm run init`.

### Running
These commands assume that you have docker installed and will launch the server in a container.
* *Install:* `npm install`
* *Default:* `npm run start`
* *Run Test Suite:* `npm run test`


### Running with Docker
These commands assume that you have docker installed and will launch the server in a container.
* *Build:* **Required to build at least once if first time running.** Use `make build`.
* *Default:* `make run`.
* *Run Test Suite:* `make test`

---

# REST API

---

In general, errors are returned with a 4xx status code, a body of type
`application/json` and a JSON response with a single element, "error",
containing the error message.

---

## `POST /quote`

### Request body:
* JSON containing:
```
{
“action”: “buy”,
“base_currency”: “USD”,
“quote_currency”: “BTC”,
“amount”: “1000.00”
}
```

### Response body:
* 200: Successfully queries order books
* 400: Bad input, see error message in response

### Example Response
```
HTTP/1.1 200 OK
{
“total”: “1.41783638”,
“price”:
“0.00147836”,
“currency”: “BTC”
}
```
