/* eslint-env node */
/* jslint devel: true, browser: true, eqeq: true, es5: true, white: true, nomen:
 * true, maxerr: 499, indent: 2 */

// Configure environment variables
var express = require('express');
var path = require('path');
var bodyParser = require('body-parser');
var app = express();
var morgan = require('morgan');

// Set up base dir
global.__base = __dirname;

// The quote string
var mainRoute = require('./routes/quote');
var router = express.Router();

// only log error responses
app.use(
    morgan('dev', {skip : function(req, res) { return res.statusCode < 400 }}));

// parse application/json
app.use(bodyParser.json())

app.all('/*', function(req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Methods', 'POST');
  next();
});

app.get('/', (req, res) => {
  return res.status(200).send('<html>"It works, NodeJS"</html>');
});

app.use('/quote', mainRoute);

// Catch 404 and forward to error handler
app.use((req, res, next) => {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// Error handlers
// Development error handler: will print stacktrace
if (app.get('env') === 'development') {
  app.use((err, req, res) => {
    res.status(err.status || 500);
    res.render('error', {message : err.message, error : err});
  });
}

// Production error handler: no stacktraces leaked to user
app.use((err, req, res) => {
  res.status(err.status || 500);
  res.render('error', {message : err.message, error : {}});
});

module.exports = app;
