#!/bin/bash
# Install clang-format
# Reads .clang-format in repo root
for jsfile in $(find . -name "*.js"|grep -v node_modules); do
    clang-format -i $jsfile;
done
