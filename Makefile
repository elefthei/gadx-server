.PHONY: build test run
SERVER := gdax-server

build:
	docker build -t ${SERVER} .

test:
	docker run -it -p 3000:3000 ${SERVER} npm run test

run:
	docker run -it -p 3000:3000 ${SERVER}

